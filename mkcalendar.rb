#!/usr/bin/env ruby
VCS_ID = -'$Id$'

require 'date'
require 'yaml'

require 'rubygems'
require 'icalendar'
require 'icalendar/tzinfo'

abort "usage: #{$PROGRAM_NAME} <calendar.yaml>\n" unless ARGV.length == 1

yaml_file = ARGV.shift
yaml_data = File.read yaml_file
data, cal = YAML.load_stream yaml_data, filename: yaml_file

TZID   = data['X-WR-TIMEZONE']
TZ     = TZInfo::Timezone.get TZID
vcaltz = nil

calendar = Icalendar::Calendar.new

def icaldate(datetime)
  Icalendar::Values::DateTime.new datetime, 'tzid' => TZID
end

def last_second(datetime)
  datetime + 1 - Rational(1, 86_400)
end

def make_event(cal, dtstart, dtend)
  cal.event do |e|
    e.dtstart     = icaldate dtstart
    e.dtend       = icaldate dtend
  end
end

def make_period_event(calendar, term_name, period)
  name    = period.keys[0]
  name    = name.gsub('-', ' ')
  name   += ' period' if name[-3..-1] == 'ing'
  dt      = period.values[0]
  dtstart = dt['start'].to_datetime
  dtend   = dt['end'].to_datetime

  event = make_event calendar, dtstart, dtend
  event.summary = "#{term_name}: #{name}"
  event
end

def make_date_event(calendar, term_name, date)
  name  = date.keys[0]
  dt    = date.values[0].to_datetime
  dtend = last_second dt

  event = make_event calendar, dt, dtend
  event.summary = "#{term_name}: #{name}"
end

cal.each do |term|
  term_name = term['term']
  dtstart   =             term['start'].to_datetime
  dtend     = last_second term['end'].to_datetime

  unless vcaltz
    vcaltz = TZ.ical_timezone dtstart
    calendar.add_timezone vcaltz
  end

  event         = make_event calendar, dtstart, dtend
  event.summary = term_name

  (term['periods'] || []).each do |period|
    make_period_event calendar, term_name, period
  end

  (term['dates'] || []).each do |date|
    make_date_event calendar, term_name, date
  end
end

puts calendar.to_ical
