########################################################################
		      UNSW Academic Calendar Dates
########################################################################

:Build (trunk):
   .. image::   https://gitlab.com/jashankj/unsw-calendars/badges/trunk/pipeline.svg
      :target:  https://gitlab.com/jashankj/unsw-calendars/commits/trunk
      :alt:     pipeline status for trunk
:Id: $Id$
:Maintainers:
   - Jashank Jeremy

.. WARNING:: This is not authoritative!

   This data is not an authoritative source
   on courses taught at CSE or at UNSW.
   Do not use it to make enrolment decisions.
   Do not assume this data
   is any way representative of UNSW.
   See UNSW's website for authoritative data.


.. WARNING:: This is not guaranteed to be correct!

   Information here is best-effort.

- ``trimesters.yaml`` → `trimesters.ics <https://gitlab.com/jashankj/unsw-calendars/-/jobs/artifacts/trunk/raw/trimesters.ics?job=build>`_ \\
  Calendar dates for trimesters (2020 --- 2021);
  derived from <https://student.unsw.edu.au/calendar>

- ``research.yaml`` → `research.ics <https://gitlab.com/jashankj/unsw-calendars/-/jobs/artifacts/trunk/raw/research.ics?job=build>`_ \\
  Calendar dates for the HDR academic calendar (2020 --- 2021);
  derived from <https://research.unsw.edu.au/hdr-academic-calendar>

- ``hexamesters.yaml`` → `hexamesters.ics <https://gitlab.com/jashankj/unsw-calendars/-/jobs/artifacts/trunk/raw/hexamesters.ics?job=build>`_
  Calendar dates for hexamesters (2020 --- 2021);
  derived from <https://student.unsw.edu.au/dates/hexamester>

Copy the links to the ``ics`` files
and add them as "web calendars"
to your favourite calendar program!
Both Outlook and Google Calendar work well.
(TODO: add better 'how-to-add-it' instructions.)

Things we want but don't yet have:

- ``canberra.yaml``:
  Calendar dates at UNSW Canberra;
  derived from <https://student.unsw.edu.au/calendar-canberra>.
  If you follow the UNSW Canberra timetable,
  please let me know how this calendar works!

- ``medicine.yaml``:
  Calendar dates for UNSW Medicine;
  derived from <https://student.unsw.edu.au/calendar-medicine>.
  If you follow the UNSW Medicine timetable,
  please let me know how this calendar works!

- ``business.yaml``:
  Calendar dates for AGSM programs;
  derived from <https://www.business.unsw.edu.au/agsm/students/timetables-and-key-dates>.
  If you follow the UNSW AGSM timetable,
  please let me know how this calendar works!

- more complete data prior to 2020
